import { NavigationContainer } from '@react-navigation/native';
import { Text } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { Screen } from '../constants';
import { Home } from '../screens';

const Stack = createNativeStackNavigator()

const linking = {
  prefixes: ['growlife://', 'https://growlife.com', 'https://*.growlife.com'],
  config: {
    screens: {
      Chat: 'feed/:sort',
      Profile: 'user',
      // Add other screens here
    },
  },
};

function RootNavigation({ route }: any) {
  return (
    <NavigationContainer linking={linking} fallback={<Text>Loading...</Text>}>
      <Stack.Navigator
        initialRouteName={route}
        screenOptions={{
          headerShown: false,
          statusBarColor: 'gray',
          statusBarStyle: 'light',
        }}>
        <Stack.Screen name={Screen.home} component={Home} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default RootNavigation